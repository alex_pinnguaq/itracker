<?php
include "header.php";
include "helpers.php";

$conn = db_connect();


?>
<h1>New Location</h1>

<input type='text' id='name'>
<button id="btn_submit" type="button" onclick="doLocationCreate()">Create</button>
<div id="loader" class="loader" style="display: none;"></div>
<div id="result"></div>
<script>
function doLocationCreate() {
  showLoad(true);
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      var result = JSON.parse(this.responseText);
      if( result.ret == "-1" )
      {
        document.getElementById("result").innerHTML = result.message;
      }
      else {
        document.getElementById("result").innerHTML = "Reservations Created Successfully!";
        window.location.replace("settings.php");
      }
      showLoad(false);
    }
  };
  xhttp.open("POST", "do_location_create.php", true);
  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  var sendString = "";
  sendString+="name="+encodeURIComponent(document.getElementById("name").value);
  xhttp.send(sendString);
}


function showLoad(show){
  if(show)
  {
    document.getElementById("loader").style.display = "block";
  }
  else{
    document.getElementById("loader").style.display = "none";
  }
}

</script>