<?php
include "header.php";
include "helpers.php";
include "dropdown_helper.php";

echo "<link rel='stylesheet' href='table.css'>";
$conn = db_connect();

// Create connection
$computer_id = $_GET['computer_id'];
$queryString = "Select * from computers where id=" . $computer_id;

$result = $conn->query($queryString);
if (!$result)
{
    echo "Error with the query....";
}
$row = $result->fetch_assoc();

echo "<h1 id='computer_id'>" . $row['id'] . "</h1>"; // ID in big font

?>

<div class="content">
<table>
	<colgroup>
		<col span="1" style="width: 15%" >
		<col span="1" style="width: 85%" >
	</colgroup>
<?php
$displayList = array(
    "status" => "Status",
    "type" => "Type",
    "location" => "Location",
    "distribution_date" => "Distribution Date",
    "manufacturer" => "Manufacturer",
    "notes" => "Notes",
    "date_refurbished" => "Date Refurbished"
);
$keyListStr = "";
foreach ($displayList as $key => $value)
{
    if ($keyListStr != "")
    {
        $keyListStr .= ",";
    }
    $keyListStr .= "'" . $key . "'";
    if ($key == "notes")
    {
        echo "<tr><td>$value</td><td><textarea rows='5' id='$key' readonly='true' >$row[$key]</textarea></td></tr>";
    }
    else if ($key == "date_refurbished" || $key == "distribution_date")
    {
        echo "<tr><td>$value</td><td><input type='date' id='$key' readonly='true' value='$row[$key]'></td></tr>";
    }
    else if ($key == "status")
    {
        echo "<tr><td>$value</td><td> " . create_status_dropdown_with_preselect($row[$key]) . " </td></tr>";
    }
    else if ($key == "location")
    {
        echo "<tr><td>$value</td><td> " . create_location_dropdown_with_preselect($row[$key]) . " </td></tr>";
    }
    else
    {


    echo "<tr><td> $value</td><td><input type='text' id='$key' readonly='true' value='$row[$key]'></td></tr>";

        }
}
?>


</table>

<div id="editWrapper"><button id="btn_edit" type="button"  >Edit</button></div>
<div id="updateWrapper" style="display: none;"><button id="btn_update" type="button" >Update</button></div>
<div id="loader" class="loader" style="display: none;"></div>
<div id="result"></div>

<br><br>
<button id="delete_button" type="button">Delete Computer</button>
<input type="checkbox" id="delete_confirm_checkbox" value="confirmed"></input>
<label for="delete_confirm_checkbox">Confirm Delete</label>
<br><br>
<span id="delete_return_message"></span>
<h2 id="result"> </h2>
<div id="loader" class="loader" style="display: none;"></div>
</div>



<script>
var elements = [<?php echo $keyListStr; ?>];


document.getElementById("btn_edit").addEventListener("click", function() {doEdit()});
document.getElementById("btn_update").addEventListener("click", function() {doComputerUpdate()});
document.getElementById("delete_button").addEventListener("click", function() {doComputerDelete()});
document.getElementById("status").disabled = true;
document.getElementById("location").disabled = true;

function doComputerUpdate() {
  //showLoad(true);
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      //document.getElementById("response").innerHTML = this.responseText;
      var result = JSON.parse(this.responseText);
      // document.getElementById("result").innerHTML = result;
      // return;
      if( result.ret == "-1" )
      {
        document.getElementById("result").innerHTML = result.message;
      }
      else {
        document.getElementById("result").innerHTML = "Computer Details Updated Successfully!";
        window.location.replace("computer_details.php?computer_id="+result.ret);
      }
      showLoad(false);
    }
  };
  xhttp.open("POST", "update_computer_details.php", true);
  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  var sendString = "";
  sendString+="&computer_id="+encodeURIComponent(<?PHP echo $computer_id ?>);
  sendString+="&status="+encodeURIComponent(document.getElementById("status").value);
  sendString+="&type="+encodeURIComponent(document.getElementById("type").value);
  sendString+="&location="+encodeURIComponent(document.getElementById("location").value);
  sendString+="&distribution_date="+encodeURIComponent(document.getElementById("distribution_date").value);
  sendString+="&manufacturer="+encodeURIComponent(document.getElementById("manufacturer").value);
  sendString+="&notes="+encodeURIComponent(document.getElementById("notes").value);
  sendString+="&date_refurbished="+encodeURIComponent(document.getElementById("date_refurbished").value);

  
  xhttp.send(sendString);
}

function doComputerDelete(){
	
	if(!document.getElementById("delete_confirm_checkbox").checked){
		document.getElementById("delete_return_message").innerHTML = "must confirm deletion with checkbox";
		return;
	}
	
	//showLoad(true);
	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() {
	if (this.readyState == 4 && this.status == 200) { 
	document.getElementById("delete_return_message").innerHTML = this.responseText;
	  var result = JSON.parse(this.responseText);
	  if( result.ret == "-1" )
	  {
		document.getElementById("delete_return_message").innerHTML = result.message;
	  }
	  else {
		document.getElementById("delete_return_message").innerHTML = "Computer Deleted Successfully!";
		setTimeout(function()  {  window.location.replace("computers_list.php");}, 3000);
		
	  }
	  showLoad(false);
	}
	};
	xhttp.open("POST", "do_computer_delete.php", true);
	xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	var sendString = "";
	sendString+="&computer_id="+encodeURIComponent(<?PHP echo $computer_id ?>);
	sendString+="&delete_confirm_checkbox="+encodeURIComponent(document.getElementById("delete_confirm_checkbox").checked);

	

	xhttp.send(sendString);	
	
}

function showLoad(show){
	if(show)
		{
			document.getElementById("loader").style.display = "block";
		}
	else	
		{
			document.getElementById("loader").style.display = "none";
		}
   }
  


function doEdit(){
    document.getElementById("btn_update").disabled=false;
    setReadOnly(false);
    showEdit(false);
	document.getElementById("status").disabled = false;
	document.getElementById("location").disabled = false;
}


function setReadOnly(ro){
    for( var i = 0; i <7; i++){
        document.getElementById(elements[i]).readOnly = ro;
    }
	document.getElementById("status").disabled = true;
	document.getElementById("location").disabled = true;
}



function showLoad(show){
  if(show)
  {
    document.getElementById("loader").style.display = "block";
  }
  else{
    document.getElementById("loader").style.display = "none";
  }
}

function showEdit(show){
  if(show)
  {
    document.getElementById("editWrapper").style.display = "inline";
    document.getElementById("updateWrapper").style.display = "none";
  }
  else{
    document.getElementById("editWrapper").style.display = "none";
    document.getElementById("updateWrapper").style.display = "inline";
  } 
}

</script>
