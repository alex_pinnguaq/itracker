
<?php
include "header.php";
include "helpers.php";


if( !array_key_exists("id", $_GET ))
{
    echo "User not found.";
    exit;
}

$userId = $_GET['id'];

if( getUserId() != $userId && getRole() != 0 ){

    echo "You do not have permission to access this page.";
    exit;
}

$queryString = "SELECT * FROM users WHERE id = ".$userId;

$conn = db_connect();

$result = $conn -> query($queryString);
if( !$result)
{
  echo "Error with the query....";
  exit;
}
else if( mysqli_num_rows($result) == 0 ){
    echo "User not found.";
    exit;
}

$userName = "";
$fName = "";
$lName = "";


while ($row = $result -> fetch_assoc()) {
    $lname = $row['lname'];
    $fname = $row['fname'];
    $userName = $row['username'];
    $status = $row['status'];
}


?>
<script>

var uid = <?php echo $userId; ?>;

function doUpdate(){
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      if( this.responseText == "-1" )
      {
        printError("Unable to Update User.");
      }
      else if (this.responseText == "1" ){
        printError("User Updated Successfully");
      }
      else {
          printError(this.responseText);
      }
    }
  };
  xhttp.open("POST", "user_do_update.php", true);
  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  var sendString = "fname="+encodeURIComponent(document.getElementById("fname").value);
  sendString+="&lname="+encodeURIComponent(document.getElementById("lname").value);
  sendString+="&id="+encodeURIComponent(uid);
  <?php if(getRole()==0){ ?>
  sendString+="&status="+encodeURIComponent(document.getElementById("status").value);
  <?php } ?>
  
  xhttp.send(sendString);
}

function updatePassword(){
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      if( this.responseText == "-1" )
      {
        printError("User not specified.");
      }
      else if (this.responseText == "-2"){
          printError("You do not have permission to perform this action.");
      }
      else if (this.responseText == "-3"){
          printError("You do not have permission to perform this action.");
      }
      else if (this.responseText == "1" ){
        printError("Password Reset.");
      }
      else {
          printError(this.responseText);
      }
    }
  };
  xhttp.open("POST", "user_do_update.php", true);
  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  var sendString = "pass="+encodeURIComponent(document.getElementById("pass").value);
  sendString+="&id="+encodeURIComponent(uid);
  xhttp.send(sendString);
}

function printError(errorString)
{
  document.getElementById("response").innerHTML = errorString;
}


</script>


<form class="create_user">
Email:<input type="text" id="email" value="<?php echo $userName;?>" readonly><br>
First Name:<input type="text" id="fname" value="<?php echo $fname;?>"><br>
Last Name:<input type="text" id="lname" value="<?php echo $lname;?>"><br>
<?php
if (getRole() == 0){
?>

Status:<select id="status">
        <option value="0" <?php if($status==0){echo "selected='selected'";}?>>Active</option>
        <option value="-1" <?php if($status==-1){echo "selected='selected'";}?>>Disabled</option>
</select>
<?php } ?>
<button type="button" onclick="doUpdate()" >Update Details</button><br>
New Password:<input type="text" id="pass" ><br>
<button type="button" onclick="updatePassword()" >Reset Password</button><br>
</form>
<div id="response"></div>

<script>

</script>