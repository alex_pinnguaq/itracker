<?php 

	/*
		Delete a computer given an id
	*/
	
	include "helpers.php";

	$conn = db_connect();
	
	$id = $_POST['computer_id'];
	
	
	$query = "DELETE FROM computers WHERE id=$id";
	$result = $conn->query($query);
	
	$is_checkbox_checked = ($_POST['delete_confirm_checkbox'] == "true");
	
if (!$result || !$is_checkbox_checked )
	{
		//echo "Insertion error: ".$conn->error." Query:".$query;
		$resArray['ret'] = - 1;
		if(!$is_checkbox_checked){
			$resArray['message'] = "Error : Must check delete confirmation checkbox. Current checkbox value : $is_checkbox_checked";
		}
		else{$resArray['message'] = "error: " . $conn->error . " Query:" . $query;}
	}	

else{
	$resArray['ret'] = $id;
	$resArray['message'] = "Success";
}

	

	echo json_encode($resArray);
	return;

?>