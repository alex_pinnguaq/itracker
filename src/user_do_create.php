<?php
session_start();

include "helpers.php";

 $conn = db_connect();

 //print_r($_POST);

 $result = -1;

 $fName = $lName = $newUserName = NULL;
 if(array_key_exists('username', $_POST))
 {

    $userName = mysqli_real_escape_string($conn, $_POST["username"]);
    $pass = mysqli_real_escape_string($conn, $_POST["pass"]);
    $fname = mysqli_real_escape_string($conn, $_POST["fname"]);
    $lname = mysqli_real_escape_string($conn, $_POST["lname"]);
    $regday = mysqli_real_escape_string($conn, $_POST["regday"]);
    $passHash = hash("md5", $pass);

    $queryString = "SELECT * from users where username = '".$userName."' ";

    //echo $queryString;
    $result = $conn -> query( $queryString);
    if( !$result){
        echo $conn->error;
        return;
    }
    if( mysqli_num_rows($result) > 0 )
    {
        $result = -2;
        echo $result;
        return;
    }

    if( $regday >= 0){
        $queryString = "SELECT * from users where regday = ".$regday." ";
        $result = $conn -> query( $queryString);
        if( !$result){
            echo $conn->error;
            return;
        }
        if( mysqli_num_rows($result) > getMaxDayUsers() )
        {
            $result = -3;
            echo $result;
            return;
        }
    
    }

    $queryString = "INSERT INTO users (username, fname, lname, password, role, status, regday) VALUES ('".$userName."','".$fname."','".$lname."','".$passHash."',1, 0, ".$regday.")";
    //echo $queryString;
    if ($conn->query($queryString) === TRUE) {
        $result = 1;
    }
    else
    {
        $result = $conn->error;
    }
}
echo $result;

?>
