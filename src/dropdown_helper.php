<?php

//hardcoded status array
$statusDropdownItems = array(
    1 => "Unassessed",
    2 => "Basic Software Installation",
    3 => "Special Software Installation",
    4 => "Final Check",
    5 => "Broken",
    6 => "Scrapped",
    7 => "On Loan",
    8 => "Ready for Distribution",
    9 => "Distributed"
);
$conn = db_connect();

// query database and put every dropdown into arrays
$queryString = "SELECT name FROM location";
$request = $conn->query($queryString);
$locationDropdownItems = array();

while ($row = $request->fetch_row())
{

    array_push($locationDropdownItems, $row[0]);

}

function create_status_dropdown()
{
    global $statusDropdownItems;
    //returns html string that creates dropdown menu with all status options
    $return_string = "";
    $return_string .= "<select id='status'>";

    foreach ($statusDropdownItems as $item)
    {
        $return_string .= "<option value ='" . $item . "'>" . $item . "</option>";
    }
    $return_string .= "</select>";
    return $return_string;
}

function create_status_dropdown_with_preselect($preselect)
{
    global $statusDropdownItems;
    //returns html string that creates dropdown menu with all status options
    $return_string = "";
    $return_string .= "<select id='status'>";

    foreach ($statusDropdownItems as $item)
    {
        if ($item == $preselect)
        {
            $return_string .= "<option value ='" . $item . "' selected>" . $item . "</option>";
        }
        else
        {
            $return_string .= "<option value ='" . $item . "'>" . $item . "</option>";
        }
    }
    $return_string .= "</select>";
    return $return_string;
}

function create_location_dropdown()
{
    global $locationDropdownItems;
    if (isset($locationDropdownItems))
    {
        //returns html string that creates dropdown menu with all location options
        $return_string = "";
        $return_string .= "<select id='location'>";

        foreach ($locationDropdownItems as $item)
        {
            $return_string .= "<option value ='" . $item . "'>" . $item . "</option>";
        }
        $return_string .= "</select>";
        return $return_string;
    }
    else return "ERROR : Could not query location table";
}

function create_location_dropdown_with_preselect($preselect)
{
    global $locationDropdownItems;
    if (isset($locationDropdownItems))
    {
        //returns html string that creates dropdown menu with all location options
        $return_string = "";
        $return_string .= "<select id='location'>";

        foreach ($locationDropdownItems as $item)
        {
            if ($item == $preselect)
            {
                $return_string .= "<option value ='" . $item . "' selected>" . $item . "</option>";
            }
            else
            {
                $return_string .= "<option value ='" . $item . "'>" . $item . "</option>";
            }
        }
        $return_string .= "</select>";
        return $return_string;
    }
    else return "ERROR : Could not query location table";
}

?>
