<?php

include "helpers.php";

$conn = db_connect();

$valueNameList = ["requestId", "computerIds"];
$resArray = array();
$debug = false;
$cols = "";
$vals = array();
for( $i = 0; $i<count($valueNameList); $i++){
    if( !array_key_exists($valueNameList[$i], $_POST)){
        //echo "Missing post arg: ".$valueNameList[$i];
        $resArray['ret']=-1;
        $resArray['message']="Missing post arg: ".$valueNameList[$i];
        echo json_encode($resArray);
        return;
    }
    $cols.=$valueNameList[$i];
    array_push($vals, $conn->real_escape_string($_POST[$valueNameList[$i]]) );
}

$ids = explode(",", $vals[1]);

$query = "UPDATE computers SET request_id = ".$vals[0]." WHERE (";
for( $i = 0; $i < count($ids); $i++){
    $query.="id = ".$ids[$i];
    if( $i < count($ids)-1){
        $query.=" OR ";
    }
}
$query.=")";


$result = $conn->query($query);
if( !$result){
    //echo "Insertion error: ".$conn->error." Query:".$query;
    $resArray['ret']=-1;
    $resArray['message']="Insertion error: ".$conn->error." Query:".$query;
    echo json_encode($resArray);
    return;
}

$resArray['ret']=$conn->insert_id;
$resArray['message']="Success with Query ".$query;
echo json_encode($resArray);
return;