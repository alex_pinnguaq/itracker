<?php
include "helpers.php";

$conn = db_connect();

$valueNameList = ["status", "type", "location", "distribution_date", "notes", "manufacturer", "date_refurbished"];
$valuesToBeChangedList = [];
$resArray = array();
$debug = false;
$cols = "";
$vals = [];
for ($i = 0;$i < count($valueNameList);$i++)
{
    if (!array_key_exists($valueNameList[$i], $_POST))
    {
        echo "Missing post arg: " . $valueNameList[$i];
        $resArray['ret'] = - 1;
        $resArray['message'] = "Missing post arg: " . $valueNameList[$i];
        echo json_encode($resArray);
        return;
    }
    else
    {

        //if value is not blank, add to array valuesToBeChangedList
        if (($valueNameList[$i] == "distribution_date" || $valueNameList[$i] == "date_refurbished") && $_POST[$conn->real_escape_string($valueNameList[$i]) ] == '')
        {
            //if value is date type and has no changes to be made to it, then do not add it to the query
            
        }
        else
        {
            array_push($valuesToBeChangedList, $valueNameList[$i]);
            array_push($vals, $_POST[$conn->real_escape_string($valueNameList[$i]) ]);
        }
    }
    $cols .= $valueNameList[$i];
    if ($_POST[$valueNameList[$i]] != "")
    {
        if ($i < count($valueNameList) - 1 && $i > 0)
        {
            $cols .= ",";

        }

    }
}

$query_bit = "";

for ($i = 0;$i < count($valuesToBeChangedList);$i++)
{
    $query_bit .= $valuesToBeChangedList[$i] . "='" . $vals[$i] . "'";
    if ($i < count($valuesToBeChangedList) - 1)
    {
        $query_bit .= ",";
    }

}
$query2 = "SELECT id FROM location WHERE name = '". $_POST['location'] ."'";
$result = $conn->query($query2);
$temp = $result -> fetch_assoc();
$location_id = $temp['id'];

$query = "UPDATE computers SET " . $query_bit . ",location_id=$location_id where id= " . $_POST['computer_id'];

$result = $conn->query($query);
if (!$result)
{
    //echo "Insertion error: ".$conn->error." Query:".$query;
    $resArray['ret'] = - 1;
    $resArray['message'] = "Insertion error: " . $conn->error . " Query:" . $query;
    echo json_encode($resArray);
    return;
}

$resArray['ret'] = $_POST['computer_id'];
$resArray['message'] = "Success";
echo json_encode($resArray);
return;
?>
