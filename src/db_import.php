<?php
echo "This script has been disable.";
return;
include "helpers.php";
$conn = db_connect();

$myfile = fopen("inventory.csv", "r") or die("Unable to open file!");


function getConstituent($conn, $constituentName){
    $query = "SELECT * FROM constituent WHERE fname = '".$conn->real_escape_string($constituentName)."'";
    $result = $conn->query($query);
    if( !$result ){
        echo "SQL Error.. ". $conn -> error." query: ".$query;
    }
    $constituentId = -1;
    if( mysqli_num_rows($result) == 0){
        echo "Doing constituent insertion";
        $query = "INSERT INTO constituent (fname) VALUES ('".$conn->real_escape_string($constituentName)."')";
        $result = $conn->query($query);
        if( !$result ){
            echo "SQL Error.. ". $conn -> error." query: ".$query;
        }
        $constituentId = $conn->insert_id;
    }
    else{
        $row = $result->fetch_assoc();
        $constituentId = $row['id'];
    }
    return $constituentId;
}

function getRequest($conn, $requestNotes, $requestedBy){
    $query = "SELECT * FROM request WHERE notes = '".$requestNotes."' AND requested_by = ".$requestedBy;
    $result = $conn->query($query);
    if( !$result ){
        echo "SQL Error.. ". $conn -> error." query: ".$query;
    }
    $requestId = -1;
    if( mysqli_num_rows($result) == 0){
        $query = "INSERT INTO request (notes, status, requested_by, number) VALUES ('".$requestNotes."', 5, ".$requestedBy.", 1)";
        $result = $conn->query($query);
        if( !$result ){
            echo "SQL Error.. ". $conn -> error." query: ".$query;
        }
        $requestId = $conn->insert_id;
    }
    else{
        $row = $result->fetch_assoc();
        $requestId = $row['id'];
    }
    return $requestId;
}

$query = "DELETE FROM computers WHERE 1"; 
$result = $conn->query($query);
if( !$result ){
    echo "SQL Error.. ". $conn -> error." query: ".$query;
    return;
}

$query = "DELETE FROM request WHERE 1"; 
$result = $conn->query($query);
if( !$result ){
    echo "SQL Error.. ". $conn -> error." query: ".$query;
    return;
}

$query = "DELETE FROM constituent WHERE 1"; 
$result = $conn->query($query);
if( !$result ){
    echo "SQL Error.. ". $conn -> error." query: ".$query;
    return;
}

while( $row = fgetcsv($myfile) ){

    print_r($row);
    // UPDATE CONSTITUENT
    $requestId = "";
    $requesterId = "";
    if( $row[9] ){
        $requesterId = getConstituent($conn, $row[9]);
        $requestId = getRequest($conn, $row[8], $requesterId);
    }
    $donorId = "";
    if( $row[5] ){
        $donorId = getConstituent($conn, $row[5]);
    }
    
    $location = getLocationStringToId($conn, $row[3]);
    $query = "INSERT INTO computers (id, status, type, location_id, date_received, received_from, distribution_date, request_id, notes, manufacturer) ";
    $query.= "VALUES (".$row[0].", '".$row[1]."', '".$row[2]."', ".$location.", '".$row[4]."', ".$donorId.", ".($row[6]?"'".$row[6]."'":"null").", ".($requestId?$requestId:"null")."";
    $query.= ", '".$conn->real_escape_string($row[7])."', '".$row[10]."' )";
    $result = $conn->query($query);
    if( !$result ){
        echo "SQL Error.. ". $conn -> error." query: ".$query;
        return;
    }

    
}

