<?php

include "header.php";
include "helpers.php";
include "constituent_helpers.php";

function selectRequestUser($conn){
  $outStr = "<p>Select a constituent to make a request.</p>";

  $queryString = "SELECT * FROM constituent ORDER BY id";
  $result = $conn->query($queryString);
  
  if( !$result ){
    echo "Error in query results.";
  }

  $keyToString = getConstituentKeyToStringArray();
  $outStr = "<table><tr>";
  $row = $result -> fetch_assoc();
  $keys = array_keys($row);
  for( $i = 0; $i < count($keys); $i++){
      $outStr .= "<td>".$keyToString[$keys[$i]]."</td>";
  }
  $outStr .= "<td>Action</td>";
  $outStr .= "</tr>";
  while( $row ){
      $outStr .="<tr>";
      for( $i = 0; $i < count($keys); $i++){
              $outStr .= "<td>".$row[$keys[$i]]."</td>";
      }
      $outStr.="<td><a href='request_create.php?userId=".$row['id']."'>Create Request</a></td>";
      $outStr.="</tr>";
      $row = $result->fetch_assoc();
  }
  $outStr.="</table>";
  return $outStr;
}

$conn = db_connect();


if( !array_key_exists("userId", $_GET) ){
  echo selectRequestUser($conn);
  return;
}

$userId = $conn->real_escape_string($_GET["userId"]);

$queryString = "SELECT * FROM constituent WHERE id = ".$userId;
$result = $conn->query($queryString);
if( !$result || $result->num_rows == 0){
  echo selectRequestUser($conn);
}


$queryString = "Select * from constituent WHERE id = ".$userId;


$result = $conn-> query($queryString);
if( !$result ){
    echo "SQL Error.. ". $conn -> error;
}
$row = $result->fetch_assoc();
$targetUser = $row["fname"]." ".$row["lname"];

?>



<form>
<p>Request For: <input type="text" value="<?php echo $targetUser;?>" readonly><a href="request_create.php">Change</a></p>
<input type="hidden" id="constituent_id" value="<?php echo $userId; ?>">
<p>Request Date: <input type="date" name = "date_requested" id="date_requested"></p>
<p>Request Deadline: <input type="date" name = "deadline" id="deadline"></p>
<p>Number: <input type="text" name = "number" id="number"></p>
<p>Notes: <textarea id="notes" name="notes" rows="4" cols="50"></textarea></p>

<button id="btn_submit" type="button" onclick="doConstituentCreate()">Create</button>
</form>
<div id="loader" class="loader" style="display: none;"></div>
<div id="result"></div>
<script>
function doConstituentCreate() {
  showLoad(true);
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      //document.getElementById("response").innerHTML = this.responseText;
      var result = JSON.parse(this.responseText);
      // document.getElementById("result").innerHTML = result;
      // return;
      if( result.ret == "-1" )
      {
        document.getElementById("result").innerHTML = result.message;
      }
      else {
        document.getElementById("result").innerHTML = "Constituent Created Successfully!";
        window.location.replace("request.php?id="+result.ret);
      }
      showLoad(false);
    }
  };
  xhttp.open("POST", "do_request_create.php", true);
  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  var sendString = "";
  sendString+="&status="+encodeURIComponent("1");
  sendString+="&requested_by="+encodeURIComponent(document.getElementById("constituent_id").value);
  sendString+="&requested_date="+encodeURIComponent(document.getElementById("date_requested").value);
  sendString+="&request_deadline="+encodeURIComponent(document.getElementById("deadline").value);
  sendString+="&number="+encodeURIComponent(document.getElementById("number").value);
  sendString+="&notes="+encodeURIComponent(document.getElementById("notes").value);
  xhttp.send(sendString);
}

function showLoad(show){
  if(show)
  {
    document.getElementById("loader").style.display = "block";
  }
  else{
    document.getElementById("loader").style.display = "none";
  }
}

</script>