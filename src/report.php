<?php
include "header.php";
include "helpers.php";


echo "<link rel=\"stylesheet\" href=\"table.css\">";
$conn = db_connect();



if( !array_key_exists("from_date", $_GET) || !array_key_exists("to_date", $_GET)){
    echo "Missing Search Range Specification.";
    return;
}

$from_date = $conn->real_escape_string($_GET["from_date"]);
$to_date =  $conn->real_escape_string($_GET["to_date"]);

$query = "SELECT location.name as location, constituent.id as constituent_id, constituent.fname as constituent_fname, constituent.lname as constituent_lname FROM ((computers INNER JOIN request ON computers.request_id = request.id) INNER JOIN constituent on request.requested_by = constituent.id) LEFT JOIN location on location.id = computers.location_id ";
$query.= "WHERE distribution_date >= '".$from_date."' and distribution_date <= '".$to_date."'";
$result = $conn->query($query);

if( !$result ){
    echo "SQL Error.. ". $conn -> error." query: ".$query;
}

$distributionCount =0;
$communities = array();
$constituents = array();
while( $row = $result->fetch_assoc()){
    //print_r($row);
    $distributionCount++;
    $constituentId = $row['constituent_id'];
    if( array_key_exists($constituentId, $constituents) ){
        $constituents[$constituentId][0]++;
    }
    else{
        $constituents[$constituentId] = [1, $row['constituent_fname']." ".$row['constituent_lname']];
    }
    $location = $row['location'];
    if( array_key_exists($location, $communities)){
        $communities[$location]++;
    }
    else{
        $communities[$location] = 1;
    }
}

echo "<h1>Report ".$from_date." - ".$to_date."<h1>";

?>

<h2>Distributions ( <?php echo $distributionCount; ?> )</h2>

<h3>Constituents</h3>
<table>
<?php
foreach( $constituents as $key=>$value)
{
    ?>
    <tr><td><a href='constituent.php?id=<?php echo $key; ?>'><?php echo $value[1]; ?></a></td><td><?php echo $value[0]; ?></td></tr>
    <?php 
}
?>
</table>

<h3>Communities</h3>
<table>
<?php
foreach( $communities as $key=>$value){
    ?>
    <tr><td><?php echo $key; ?></td><td><?php echo $value; ?></td></tr>
    <?php
}
?>
</table>



<h2>Donations</h2>


<?php

$query = "SELECT computers.id, constituent.id as constituent_id, constituent.fname as constituent_fname, constituent.lname as constituent_lname FROM computers INNER JOIN constituent ON constituent.id = computers.received_from WHERE date_received >= '".$from_date."' and date_received <= '".$to_date."'";
$result = $conn->query($query);

if( !$result ){
    echo "SQL Error.. ". $conn -> error." query: ".$query;
}

$donationCount =0;
$constituents = array();
while( $row = $result->fetch_assoc()){
    //print_r($row);
    $distributionCount++;
    $constituentId = $row['constituent_id'];
    if( array_key_exists($constituentId, $constituents) ){
        $constituents[$constituentId][0]++;
    }
    else{
        $constituents[$constituentId] = [1, $row['constituent_fname']." ".$row['constituent_lname']];
    }
}

?>


<h3>Constituents</h3>
<table>
<?php

foreach( $constituents as $key=>$value)
{
    ?>
    <tr><td><a href='constituent.php?id=<?php echo $key; ?>'><?php echo $value[1]; ?></a></td><td><?php echo $value[0]; ?></td></tr>
    <?php 
}
?>
</table>




<h2>Refurbishments</h2>


<?php

$query = "SELECT COUNT(computers.id) as refurbs FROM computers WHERE date_refurbished >= '".$from_date."' and date_refurbished <= '".$to_date."'";
$result = $conn->query($query);
if( !$result ){
    echo "SQL Error.. ". $conn -> error." query: ".$query;
}

$refurbs = $result->fetch_assoc()['refurbs']
?>
<div class='content'>Total refurbishments: <?php echo $refurbs; ?></div>