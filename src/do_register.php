<?php
session_start();

include "helpers.php";

$conn = db_connect();

 //print_r($_POST);

 $fName = $lName = $newUserName = NULL;
 if(array_key_exists('dayId', $_POST) && array_key_exists('userId', $_SESSION))
 {
    
    $userId = $_SESSION['userId'];
    $dateId =  mysqli_real_escape_string($conn, $_POST["dayId"]);

  if( getUserStatus($conn, $userId) != 0)
  {
    echo -8;
    return;
  }

    $queryString = "SELECT * FROM date WHERE id = ".$dateId;

    $result = $conn->query($queryString);
    if( mysqli_num_rows($result) == 0 )
    {
      echo -5;
      return;
    }


    if( $row = $result->fetch_assoc())
   {
      if( $row['date'] < date('Y-m-d') || $row['end']< date('H:i:s') ){
        echo -6;
        return;
      }
   }

   $queryString = "SELECT * FROM registration INNER JOIN date ON registration.date_id = date.id WHERE registration.user_id = ".$userId." AND date.id = ".$dateId;

   $result = $conn->query($queryString);
   if( mysqli_num_rows($result) > 0 )
   {
     echo -7;
     return;
   }


    $queryString = "SELECT * FROM registration INNER JOIN date ON registration.date_id = date.id WHERE date.date >= '".date('Y-m-d')."' and registration.user_id = ".$userId;

    $result = $conn->query($queryString);
    if( mysqli_num_rows($result) > 0 && getRole()!= 0 )
    {
      echo -2;
      return;
    }

    $queryString = "SELECT * FROM registration INNER JOIN date ON registration.date_id = date.id WHERE date.date = CURDATE()";
    $result = $conn->query($queryString);
    if( mysqli_num_rows($result) > getMaxRegistration() )
    {
      echo -1;
      return;
    }

    $queryString = "INSERT INTO registration (date_id, user_id) values (".$dateId.", ".$userId.")";
    $result = $conn->query($queryString);
    if( !$result ){
        echo -3;
        return;
    }
    echo 1;
    return;
}
//echo -4;
echo "dayID: ".$_POST['dayId']." Session".$_SESSION['userId'];


?>
