<?php

include "header.php";
include "helpers.php";
include "request_helpers.php";

echo "<link rel=\"stylesheet\" href=\"table.css\">";
$conn = db_connect();

// Create connection

if( !array_key_exists("id", $_GET) ){
    echo "No user id specified.";
    return;
}

$userId = $conn->real_escape_string($_GET["id"]);

$queryString = "Select * from constituent WHERE id=".$userId;

$result = $conn-> query($queryString);
if( !$result ){
    echo "SQL Error.. ". $conn -> error." query: ".$queryString;
    return;
}
if( mysqli_num_rows($result) == 0){
    echo "User not found";
    return;
}
$row = $result->fetch_assoc();
$name = $row['fname']." ".$row['lname'];


?>
<h1><?php echo $name;?></h1>
<h2>Details</h2>
<div class="content">
<table>
<?php
$displayList = array("fname"=>"First Name", "lname"=>"Last Name", "email"=>"Email", "phone"=>"Phone", "address"=>"Address");
$keyListStr = "";
foreach( $displayList as $key=>$value){
    if( $keyListStr != ""){
        $keyListStr.=",";
    }
    $keyListStr.="'".$key."'";
    ?>
    <tr><td><?php echo $value; ?></td><td><input type='text' id='<?php echo $key;?>' readonly='true' value='<?php echo $row[$key]; ?>'></td></tr>

    <?php
}
?>
</table>

<div id="editWrapper"><button id="btn_edit" type="button" onclick="doEdit()">Edit</button></div>
<div id="updateWrapper" style="display: none;"><button id="btn_update" type="button" onclick="doUpdate()">Update</button></div>
<div id="loader" class="loader" style="display: none;"></div>
<div id="result"></div>
</div>
<script>
var elements = [<?php echo $keyListStr; ?>];


var completeStatusKey = "<?php echo getKeyFromStatus("Complete"); ?>";
function doEdit(){
    document.getElementById("btn_update").disabled=false;
    setReadOnly(false);
    showEdit(false);
}

function setReadOnly(ro){
    for( var i = 0; i < elements.length; i++){
        document.getElementById(elements[i]).readOnly = ro;
    }
}

function onStatusChange(){
    var status = document.getElementById("status").value;
    if( status == completeStatusKey){
        document.getElementById("row_update_computer_status").style.display = "table-row";
    }
    else{
        document.getElementById("row_update_computer_status").style.display = "none";
    }
}

function doUpdate(){
    showLoad(true);
    setReadOnly(true);
    document.getElementById("btn_update").disabled=true;

  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      //document.getElementById("response").innerHTML = this.responseText;
      var result = JSON.parse(this.responseText);
      // document.getElementById("result").innerHTML = result;
      // return;
      if( result.ret == "-1" )
      {
        document.getElementById("result").innerHTML = result.message;
        setReadOnly(false);
      }
      else {
        document.getElementById("result").innerHTML = "Request Updated Successfully!";
        showEdit(true);
      }
      showLoad(false);
    }
  };
  xhttp.open("POST", "do_constituent_update.php", true);
  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  var sendString = "id=<?php echo $userId; ?>";
  for( var i = 0; i < elements.length; i++){
      sendString +="&"+elements[i]+"="+encodeURIComponent(document.getElementById(elements[i]).value);
  }
  xhttp.send(sendString);
}

function showLoad(show){
  if(show)
  {
    document.getElementById("loader").style.display = "block";
  }
  else{
    document.getElementById("loader").style.display = "none";
  }
}

function showEdit(show){
  if(show)
  {
    document.getElementById("editWrapper").style.display = "inline";
    document.getElementById("updateWrapper").style.display = "none";
  }
  else{
    document.getElementById("editWrapper").style.display = "none";
    document.getElementById("updateWrapper").style.display = "inline";
  } 
}

</script>








<h2>Requests</h2>
<div class='content'>
<a href='request_create.php?userId=<?php echo $userId; ?>'>Create Request</a>

<?php
$queryString = "SELECT * FROM request WHERE requested_by = ".$userId;
$result = $conn->query($queryString);

if( !$result){
    echo SQLErrorToString($queryString, $conn);
}

if( mysqli_num_rows($result) > 0){
    ?>
    
    <table><tr><th>Id</th><th>Status</th><th>Requested Date</th><th>Request Deadline</th><th>Number</th><th>Notes</th></tr>
    <?php
    while( $row = $result->fetch_assoc()){
        ?>

        <tr><td><a href='request.php?id=<?php echo $row['id'];?>'><?php echo $row['id']; ?></a></td>
        <td><?php echo requestStatusToString( $row['status']); ?></td>
        <td><?php echo $row['requested_date']; ?></td>
        <td><?php echo $row['request_deadline']; ?></td>
        <td><?php echo $row['number']; ?></td>
        <td><?php echo $row['notes']; ?></td></tr>
        
        <?php

    }
    ?>
    </table>
    <?php
}
?>
</div>
<h2>Donations</h2>
<div class='content'>
<a href='insert_computer_form.php?userId=<?php echo $userId; ?>'>Record Donation</a>

<?php
$queryString = "SELECT * FROM computers WHERE received_from = ".$userId;
$result = $conn->query($queryString);

if( !$result){
    echo SQLErrorToString($queryString, $conn);
}

if( mysqli_num_rows($result) > 0){
    ?>
    
    <table><tr><th>Id</th><th>Status</th><th>Type</th><th>Date Received</th>
    <?php
    while( $row = $result->fetch_assoc()){
        ?>

        <tr><td><a href='computer_details.php?id=<?php echo $row['id'];?>'><?php echo $row['id']; ?></a></td>
        <td><?php echo $row['status']; ?></td>
        <td><?php echo $row['type']; ?></td>
        <td><?php echo $row['date_received']; ?></td>
        
        <?php

    }
    ?>
    </table>
    <?php
}
?>
</div>