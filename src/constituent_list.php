<?php

include "header.php";
include "helpers.php";
include "constituent_helpers.php";

echo "<link rel=\"stylesheet\" href=\"table.css\">";
$conn = db_connect();

// Create connection

$queryString = "Select * from constituent ORDER BY id";
echo "<a href='constituent_create.php'>Create</a>";

$result = $conn-> query($queryString);
if( !$result ){
    echo "SQL Error.. ". $conn -> error;
}
// $tableStr = displayContituentTable( $result);
// echo $tableStr;
// id	fname	lname	email	phone	address


$keyToString = getConstituentKeyToStringArray();


if( !$result ){
    echo "Error in query results.";
}
$outStr = "<table><tr>";
$row = $result -> fetch_assoc();
$keys = array_keys($row);
for( $i = 0; $i < count($keys); $i++){
    $outStr .= "<td>".$keyToString[$keys[$i]]."</td>";
}
$outStr .= "</tr>";
while( $row ){
    $outStr .="<tr>";
    for( $i = 0; $i < count($keys); $i++){
        if( $keys[$i] == "id"){
            $outStr.="<td><a href='constituent.php?id=".$row['id']."'>".$row['id']."</a></td>";
        }
        else{
            $outStr .= "<td>".$row[$keys[$i]]."</td>";
        }
    }
    $outStr.="</tr>";
    $row = $result->fetch_assoc();
}
$outStr.="</table>";
echo $outStr;


 ?>
