<?php
include "helpers.php";

$conn = db_connect();

$resArray = array();

if(!array_key_exists("name", $_POST) ){
    $resArray['ret']=-1;
    $resArray['message']="Missing post arg: name";
    echo json_encode($resArray);
    return;
}
$locationName = $conn->real_escape_string($_POST["name"]);

$query = "SELECT * FROM location WHERE name='".$locationName."'";
$result = $conn->query($query);

if(mysqli_num_rows($result) > 0){
    $resArray['ret']=-1;
    $resArray['message']="A location with that name already exists.";
    echo json_encode($resArray);
    return;
}


$query = "INSERT INTO location (name) VALUES ('".$locationName."')";
$result = $conn->query($query);

if( !$result ){
    $resArray['ret']=-1;
    $resArray['message']=SQLErrorToString($query, $conn);
    echo json_encode($resArray);
    return;
}

$resArray['ret']=0;
$resArray['message']="Success";
echo json_encode($resArray);
return;


?>