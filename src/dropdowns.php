<?php 

//Implement by including this file, then the dropdown values for a given dropdown group can be referred to by adding the prefix of "dropdown_"
//For Example : 
//
//		$dropdown_status will be an array containing all dropdown values for status
//		same goes for $dropdown_computer_type
//
include "helpers.php";

$conn = db_connect();


/*
*
*
*  !!! If any more dropdown menus are needed, update the database table 'dropdowns' with desired values, then add the dropdown group to this array and the rest is automatic !!!
*
*
*/


$items_that_need_dropdowns = ['status','computer_type','location']; 
$all_dropdowns = Array();


//https://www.php.net/manual/en/function.extract.php for reference
extract($items_that_need_dropdowns,EXTR_PREFIX_ALL,"dropdown_"); //make a variable for each item in $items_that_need_dropdowns

/*
*	set to add prefix of 'dropdown_' to each variable created from $items_that_need_dropdowns
*/


// Loop through each item in the $items_that_need_dropdowns array and query the database for all associated dropdown values and insert into its own array to be used in PHP
foreach($items_that_need_dropdowns as $item){ 
	
	$query = "Select value FROM dropdowns WHERE dropdown_group='" . $item . "';";
	$result = $conn->query($query);
	//echo "<p>".$query . "<p></br>";
	
	${'dropdown_' . $item} = Array();

	if( !$result){ // if error retreiving dropdown menu, only thing in dropdown menu will be 
		array_push(${'dropdown_' . $item},"Error fetching dropdown");
		echo  $conn->error . "\n\n";
	}
	else{ // if successful push the value onto the array
		while ($row = $result->fetch_row()) 
		{ 
			//echo "<p>query result :".$row[0] . "<p></br>";
			// loop through results and push each dropdown value set into corresponding 
			array_push(${'dropdown_' . $item},$row[0]);
			
		}
		var_dump(${'dropdown_' . $item}); // Uncomment to echo all dropdown array values
	}
}


?>