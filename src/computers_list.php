<?php

include "header.php";
include "helpers.php";

echo "<link rel=\"stylesheet\" href=\"table.css\">";
$conn = db_connect();

// Create connection

$queryString = "SELECT computers.id, computers.status, computers.type, computers.date_received, computers.received_from, donor.fname as donor_fname, donor.lname as donor_lname, computers.location_id, location.name as location, computers.distribution_date, computers.notes, constituent.fname, constituent.lname, constituent.id as constituent_id, computers.request_id, computers.manufacturer, computers.date_refurbished FROM ( (computers LEFT JOIN request ON request.id = computers.request_id LEFT JOIN constituent ON constituent.id = request.requested_by ) LEFT JOIN constituent as donor ON donor.id = computers.received_from ) LEFT JOIN location on location.id = computers.location_id ORDER BY computers.id";


$result = $conn -> query($queryString);
if( !$result)
{
  echo "Error with the query....";
}

//table header 
echo "<table>
<div>
<tr>
<th>id</th>
<th>status</th>
<th>type</th>
<th>location</th>
<th>distribution date</th>
<th>date received</th>
<th>received from</th>
<th>notes</th> <th>request</th>
<th>recipient</th>
<th>manufacturer</th>
<th>date refurbished</th>
</tr>
</div>";

while ($row = $result -> fetch_assoc()) {
    
	
	// set a URL for the details page and put it in each ID hyperlink
	$id_url =  "http://" . $_SERVER['SERVER_NAME'] . "/computer_details.php?computer_id=" . $row['id']; 
	$constituent = $row['fname']." ".$row['lname'];
	$constituentLink = "<a href='constituent.php?id=".$row['constituent_id']."'>".$constituent."</a>";
  $donor = $row['donor_fname']." ".$row['donor_lname'];
  $donorLink = "<a href='constituent.php?id=".$row['received_from']."'>".$donor."</a>";
  $reservationLink = "<a href='request.php?id=".$row['request_id']."'>".$row['request_id']."</a>";
	//$location = getLocationSelect($conn, "", true, $row['location_id']);
  $location = $row['location'];
  echo "<tr><td><a href=" . $id_url .">".$row['id']."</a></td><td>".$row['status']."</td><td>".$row['type']."</td><td>".$location."</td><td>".$row['distribution_date']."</td><td>".$row['date_received']."</td><td>".$donorLink."</td><td>".$row['notes']."</td><td>".$reservationLink."</td><td>".$constituentLink."</td><td>".$row['manufacturer']."</td><td>". $row['date_refurbished']."</td></tr>" ;
  }
echo "</table>";
 ?>
