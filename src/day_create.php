<?php
include "header.php";

$defaultDate = " new Date() ";

include "helpers.php";
$conn = db_connect();

$dayInfo = array();
$dayId = -1;
if(array_key_exists('id', $_GET))
{
  $dayId = $_GET["id"];
  $dayInfo = getDayInfo($conn, $_GET["id"]);
  $defaultDate = " new Date('".$dayInfo['date']."') ";
}
else {
  $queryString = "select  date, daynumber from date where date = (select max(date) from date) ";
  $result = $conn->query($queryString);
  if( $row = $result->fetch_assoc())
  {
      $dayInfo["dayNumber"] = max(1, ($row['daynumber'] + 1 )%7);
  }
  else
  {
    $dayInfo["dayNumber"] = 1;
  }
}
?>

<link rel="stylesheet" href="calendar/CalendarPicker.style.css" />
<script src="calendar/CalendarPicker.js"></script>
<script>
const myCalender = new CalendarPicker('#myCalendarWrapper', {
      // options here
});

</script>

 <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>



<form action="day_create.php" method="post">
<p>Date: <input type="text" name = "date" id="datepicker" <?php if($dayId != -1){echo "disabled";}?> ></p>
Day:
  <input type="radio" name="daynum" id="dayNum1" value="1">1
  <input type="radio" name="daynum" id="dayNum2" value="2">2
  <input type="radio" name="daynum" id="dayNum3" value="3">3
  <input type="radio" name="daynum" id="dayNum4" value="4">4
  <input type="radio" name="daynum" id="dayNum5" value="5">5
  <input type="radio" name="daynum" id="dayNum6" value="6">6

  <br>Message of the Day:
  <textarea id="message" name="message" maxlength="512" ></textarea>

  <br>Select image to upload:
  <input type="file" name="fileToUpload" id="fileToUpload">
  <button type="button" onclick="upload()">Upload</button>
  <div id="uploadResponse"></div>
  <div id="imagePreview"></div>

  <input type="hidden" id="imageId" name="imageId" value="-1">
  <input type="hidden" id="update" name="update" value="TRUE">

  <button id="btn_create" type="button" onclick="doCreateDay()"><?php echo ($dayId == -1)?"Create":"Update";?></button><div id="loader" class="loader"></div>
</form>
<script>
//var date = new Date();
    //date.setDate();

    $("#datepicker").datepicker({
        dateFormat: "yy-mm-dd",
        defaultDate: <?php echo $defaultDate;?>,
        onSelect: function () {
            selectedDate = $.datepicker.formatDate("yy-mm-dd", $(this).datepicker('getDate'));
        }
    });

    $("#datepicker").datepicker("setDate", <?php echo $defaultDate;?>);

  </script>

  <div id="result" class="message"></div>



<script >


document.getElementById("dayNum<?php echo $dayInfo["dayNumber"]; ?>").checked = true;
<?php
if(array_key_exists('message', $dayInfo))
{
  echo 'document.getElementById("message").value = "'.$dayInfo["message"].'";';
}
if(array_key_exists('image', $dayInfo))
{
  echo 'document.getElementById("imagePreview").innerHTML = "<img src=\"'.$dayInfo["image"].'\">";';
}
if(array_key_exists('date', $dayInfo))
{
  $dayInfo = getDayInfo($conn, $_GET["id"]);
}
 ?>

</script>


---------------------------------------------

<script>
function upload() {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      //document.getElementById("response").innerHTML = this.responseText;
      var result = JSON.parse(this.responseText);
      if( result.result == "-1" )
      {
        document.getElementById("uploadResponse").innerHTML = result.msg;

      }
      else {
        document.getElementById("imageId").value = result.msg;
        document.getElementById("uploadResponse").innerHTML = "Image uploaded sucessfully";
        document.getElementById("imagePreview").innerHTML="<img src='"+result.path+"'>";
      }

    }
  };
  xhttp.open("POST", "imageUpload.php", true);
  //xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  var formData = new FormData();
  var fileElement = document.getElementById("fileToUpload");
  if( fileElement.files.length == 1 )
  {
    formData.append("fileToUpload", fileElement.files[0]);
    xhttp.send(formData);
  }
}

function showLoad(show){
  if(show)
  {
    document.getElementById("loader").style.display = "block";
  }
  else{
    document.getElementById("loader").style.display = "none";
  }
}

function doCreateDay() {
  showLoad(true);
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      //document.getElementById("response").innerHTML = this.responseText;
      var result = JSON.parse(this.responseText);
      if( result.result == "-1" )
      {

        document.getElementById("result").innerHTML = "A day with that name already exists.  <a href='day_create.php?id="+(result.id).toString()+"'>Edit</a> instead?";
      }
      else if (result.result == "-2")
      {
      }

      else if(result.result == "-3"){
        document.getElementById("result").innerHTML = "Update Error.";
      }
      else if(result.result == "2"){
        document.getElementById("result").innerHTML = "Update Successful.";
      }
      else {
        document.getElementById("result").innerHTML = "Day Created Successfully!";
        window.location.replace("day_create.php?id="+result.id);
      }
      showLoad(false);
    }
  };
  xhttp.open("POST", "do_day_create.php", true);
  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

  var daynum = 1;
  var ele = document.getElementsByName('daynum');

  for(i = 0; i < ele.length; i++) {
      if(ele[i].checked){
        daynum = ele[i].value;
      }
  }

  var sendString = "daynum="+daynum;
  sendString+="&message="+encodeURIComponent(document.getElementById("message").value);
  sendString+="&date="+encodeURIComponent(document.getElementById("datepicker").value);
  sendString+="&fileToUpload="+encodeURIComponent(document.getElementById("fileToUpload").value);
  sendString+="&imageId="+encodeURIComponent(document.getElementById("imageId").value);
  sendString+="&update="+encodeURIComponent(document.getElementById("update").value);
  sendString+="&dayId="+encodeURIComponent(<?php echo $dayId; ?>);
  xhttp.send(sendString);
}
</script>

<div id="response"></div>

<script>
showLoad(false);
</script>
