<?php
include "header.php";
include "helpers.php";

$conn = db_connect();
if(!array_key_exists("id", $_GET) ){
    $resArray['ret']=-1;
    $resArray['message']="Missing post arg: id";
    echo json_encode($resArray);
    return;
}
$id = $conn->real_escape_string($_GET["id"]);

$query = "SELECT * FROM location WHERE id=".$id;
$result = $conn->query($query);

if(!$result){
    echo SQLErrorToString($query, $conn);
    return;
}
$row = $result->fetch_assoc();

?>

<h1>Location Edit</h1>

<input type='text' id='name' value='<?php echo $row['name'];?>'>
<button id="btn_submit" type="button" onclick="doLocationEdit()">Update</button>
<button id="btn_submit" type="button" onclick="doLocationDelete()">Delete</button>
<a href='settings.php'>Back</a>
<div id="loader" class="loader" style="display: none;"></div>
<div id="result"></div>
<script>
function doLocationEdit() {
  showLoad(true);
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      var result = JSON.parse(this.responseText);
      if( result.ret == "-1" )
      {
        document.getElementById("result").innerHTML = result.message;
      }
      else {
        document.getElementById("result").innerHTML = "Reservations Created Successfully!";
        window.location.replace("settings.php");
      }
      showLoad(false);
    }
  };
  xhttp.open("POST", "do_location_update.php", true);
  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  var sendString = "";
  sendString+="name="+encodeURIComponent(document.getElementById("name").value);
  sendString+="&id="+encodeURIComponent(<?php echo $id;?>);
  xhttp.send(sendString);
}

function doLocationDelete() {
  showLoad(true);
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      var result = JSON.parse(this.responseText);
      if( result.ret == "-1" )
      {
        document.getElementById("result").innerHTML = result.message;
      }
      else if (result.ret == "-1"){
        var error = "Unable to delete location. It is being used in the following computer records:<br>";
        var ids = result.ids;
        for( var i = 0; i < ids.length; i++){
            error+="<a href='computer.php?id="+ids[i]+"'>"+ids[i]+"</a><br>";
        }
        document.getElementById("result").innerHTML = error;
      }
      else {
        document.getElementById("result").innerHTML = "Reservations Created Successfully!";
        window.location.replace("settings.php");
      }
      showLoad(false);
    }
  };
  xhttp.open("POST", "do_location_update.php", true);
  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  var sendString = "";
  sendString+="id="+encodeURIComponent(<?php echo $id;?>);
  sendString+="&delete=1";
  xhttp.send(sendString);
}


function showLoad(show){
  if(show)
  {
    document.getElementById("loader").style.display = "block";
  }
  else{
    document.getElementById("loader").style.display = "none";
  }
}

</script>