<?php
include "helpers.php";

$conn = db_connect();

$resArray = array();




if(!array_key_exists("id", $_POST) ){
    $resArray['ret']=-1;
    $resArray['message']="Missing post arg: name";
    echo json_encode($resArray);
    return;
}
$id = $conn->real_escape_string($_POST["id"]);
if(array_key_exists("delete", $_POST) && $_POST["id"]="1"){
    $query = "SELECT id FROM computer WHERE location_id = ".$id;
    $result = $conn->query($query);
    if( $result && mysqli_num_rows($result)> 0 ){
        $resArray['ret']=-2;
        $resArray['message']="Location in use.";
        $ids = [];
        while( $row = $result->fetch_assoc()){
            array_push($ids, $row['id']);
        }
        $resArray['ids']=$ids;
        echo json_encode($resArray);
        return;
    }
    
    $query = "DELETE FROM location WHERE id=".$id;
    $result = $conn->query($query);
    if( !$result ){
        $resArray['ret']=-1;
        $resArray['message']=SQLErrorToString($query, $conn);
        echo json_encode($resArray);
        return;
    }
    $resArray['ret']=0;
    $resArray['message']="Success";
    echo json_encode($resArray);
    return;
}

if(!array_key_exists("name", $_POST) ){
    $resArray['ret']=-1;
    $resArray['message']="Missing post arg: name";
    echo json_encode($resArray);
    return;
}

$locationName = $conn->real_escape_string($_POST["name"]);


$query = "UPDATE location SET name='".$locationName."' WHERE id=".$id;
$result = $conn->query($query);

if( !$result ){
    $resArray['ret']=-1;
    $resArray['message']=SQLErrorToString($query, $conn);
    echo json_encode($resArray);
    return;
}

$resArray['ret']=0;
$resArray['message']="Success";
echo json_encode($resArray);
return;


?>

