<?php
include "helpers.php";
include "constituent_helpers.php";

$conn = db_connect();



$resArray = array();

$constituentFieldArray = getConstituentKeyToStringArray();
$tableValueStartIndex = 1;
$resArray = array();
$debug = false;
$keys = array();
$vals = array();
foreach( $constituentFieldArray as $key=>$value ){
    if( !array_key_exists($key, $_POST)){
        //echo "Missing post arg: ".$valueNameList[$i];
        $resArray['ret']=-1;
        $resArray['message']="Missing post arg: ".$key;
        echo json_encode($resArray);
        return;
    }
    array_push($vals, $conn->real_escape_string($_POST[$key]) );
    array_push($keys, $key);
}


$query = "UPDATE constituent SET ";

for($i = $tableValueStartIndex; $i < count($keys); $i++){
    $query.=$keys[$i]." = '".$vals[$i]."'";
    if( $i < count($keys) - 1){
        $query.=",";
    }
}
$query.=" WHERE id=".$vals[0];


$result = $conn->query($query);

if( !$result){
    //echo "Insertion error: ".$conn->error." Query:".$query;
    $resArray['ret']=-1;
    $resArray['message']="Update Error: ".$conn->error." Query:".$query;
    echo json_encode($resArray);
    return;
}

$resArray['ret']=$conn->insert_id;
$resArray['message']="Success";
echo json_encode($resArray);
return;



?>