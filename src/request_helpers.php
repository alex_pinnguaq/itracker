<?php

function getRequestKeyToStringArray(){
    return array("id"=>"Id", "status"=>"First Name", "requested_by"=>"Last Name", "requested_date"=>"Email", "request_deadline"=>"Phone", "number"=>"Address", "notes"=>"Notes");
}

$requestStatusList = array(1=>"Requested", 2=>"Accepted", 3=>"In-Progress", 4=>"Cancelled", 5=>"Complete");

function requestStatusToString($statusId){
    global $requestStatusList;
    return $requestStatusList[$statusId];
}

function getRequestStatusDropdown($elementId, $selected=-1, $readonly=false, $onChange = ""){
    global $requestStatusList;
    $outStr = "<select id=".$elementId." name='requestStatus' ".($readonly?"disabled='disabled'":"");
    if( $onChange != ""){
        $outStr .=" onchange='".$onChange."'";
    }
    $outStr .=">";
    foreach ($requestStatusList as $key => $value) {
        $outStr.="<option value='".$key."' ".($key==$selected?"selected='true'":"").">".$value."</option>";
    }
    $outStr.="</select>";
    return $outStr;
}

function getKeyFromStatus($status ){
    global $requestStatusList;
    foreach ($requestStatusList as $key => $value) {
        if( $value == $status){
            return $key;
        }
    }
    return -1;
}
?>