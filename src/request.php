<?php

include "header.php";
include "helpers.php";
include "request_helpers.php";

echo "<link rel=\"stylesheet\" href=\"table.css\">";
$conn = db_connect();

// Create connection

if( !array_key_exists("id", $_GET) ){
    echo "No request specified.";
    return;
}

$requestId = $conn->real_escape_string($_GET["id"]);   

$queryString = "SELECT request.id, request.status, request.requested_by, request.requested_date, request.request_deadline, request.number, request.notes, constituent.fname, constituent.lname FROM request INNER JOIN constituent ON request.requested_by = constituent.id WHERE request.id=".$requestId;

$result = $conn-> query($queryString);
if( !$result ){
    echo "SQL Error.. ". $conn -> error." query: ".$queryString;
}
$row = $result->fetch_assoc();
?>
<h1>Request</h1>
<h2>Details</h2>
<div class="content">
<table>
<tr><td>Request Id</td><td><?php echo $requestId;?></td></tr>
<tr><td>Requested By</td><td><a href='constituent.php?id=<?php echo $row['requested_by']; ?>'><?php echo $row['fname']." ".$row['lname']?></td></tr>
<tr><td>Status</td><td><?php echo getRequestStatusDropdown("status",$row['status'],true, "onStatusChange()");?></td></tr>
<tr id="row_update_computer_status" style="display:none;"><td></td><td>
    Mark Computers Distributed:<input type="checkbox" id="update_compuer_status" checked="true" ><br>
    Set Distribution Date:<input type="checkbox" id="set_distribution_date" checked="true" ><input type="date" id='distribution_date' value="<?php echo date("Y-m-d");?>"><br>
    *updates only applied to non-distributed computers.</td>
</tr>
<tr><td>Request Date</td><td><input type='date' id="date_requested" name="date_requesetd" value='<?php echo $row['requested_date']; ?>' readonly></td></tr>
<tr><td>Request Deadline</td><td><input type='date' id="deadline" name="deadline" value='<?php echo $row['request_deadline']; ?>' readonly></td></tr>
<tr><td>Quantity</td><td><input type='text' id="number" name="number" value='<?php echo $row['number']; ?>' readonly></td></tr>
<tr><td>Notes</td><td><textarea type='text' id="notes" name="notes" rows="4" cols="50" readonly><?php echo $row['notes']; ?></textarea></td></tr>
</table>

<div id="editWrapper"><button id="btn_edit" type="button" onclick="doEdit()">Edit</button></div>
<div id="updateWrapper" style="display: none;"><button id="btn_update" type="button" onclick="doUpdate()">Update</button></div>
<div id="loader" class="loader" style="display: none;"></div>
<div id="result"></div>
</div>
<script>
var elements = [ "date_requested", "deadline", "number", "notes"];

var completeStatusKey = "<?php echo getKeyFromStatus("Complete"); ?>";
function doEdit(){
    document.getElementById("btn_update").disabled=false;
    setReadOnly(false);
    showEdit(false);
}

function setReadOnly(ro){
    document.getElementById("status").disabled = ro;
    for( var i = 0; i < elements.length; i++){
        document.getElementById(elements[i]).readOnly = ro;
    }
}

function onStatusChange(){
    var status = document.getElementById("status").value;
    if( status == completeStatusKey){
        document.getElementById("row_update_computer_status").style.display = "table-row";
    }
    else{
        document.getElementById("row_update_computer_status").style.display = "none";
    }
}

function doUpdate(){
    showLoad(true);
    setReadOnly(true);
    document.getElementById("btn_update").disabled=true;

  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      //document.getElementById("response").innerHTML = this.responseText;
      var result = JSON.parse(this.responseText);
      // document.getElementById("result").innerHTML = result;
      // return;
      if( result.ret == "-1" )
      {
        document.getElementById("result").innerHTML = result.message;
        setReadOnly(false);
      }
      else {
        document.getElementById("result").innerHTML = "Request Updated Successfully!";
        showEdit(true);
      }
      showLoad(false);
    }
  };
  xhttp.open("POST", "do_request_update.php", true);
  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  var sendString = "";
  sendString+="&id="+encodeURIComponent(<?php echo $requestId; ?>);
  sendString+="&status="+encodeURIComponent(document.getElementById("status").value);
  sendString+="&requested_date="+encodeURIComponent(document.getElementById("date_requested").value);
  sendString+="&request_deadline="+encodeURIComponent(document.getElementById("deadline").value);
  sendString+="&number="+encodeURIComponent(document.getElementById("number").value);
  sendString+="&notes="+encodeURIComponent(document.getElementById("notes").value);
  sendString+="&updateComputers="+encodeURIComponent((document.getElementById("update_compuer_status").checked == true && document.getElementById("status").value == completeStatusKey)?"1":"0");
  sendString+="&updateComputersDate="+encodeURIComponent((document.getElementById("set_distribution_date").checked == true && document.getElementById("status").value == completeStatusKey)?"1":"0");
  sendString+="&distribution_date="+encodeURIComponent(document.getElementById("distribution_date").value);
  xhttp.send(sendString);
}

function showLoad(show){
  if(show)
  {
    document.getElementById("loader").style.display = "block";
  }
  else{
    document.getElementById("loader").style.display = "none";
  }
}

function showEdit(show){
  if(show)
  {
    document.getElementById("editWrapper").style.display = "inline";
    document.getElementById("updateWrapper").style.display = "none";
  }
  else{
    document.getElementById("editWrapper").style.display = "none";
    document.getElementById("updateWrapper").style.display = "inline";
  } 
}

</script>

<h2>Reservations</h2>
<div class="content">
<a href='reservation_create.php?requestId=<?php echo $requestId;?>'>Reserve Computers</a>
<table><tr><th></th><th>Computer Id</th><th>Status</th><th>Distribution Date</th><th>Type</th><th>Location</th></tr>

<?php 

$queryString = "SELECT * FROM computers WHERE request_id = ".$requestId;

$result = $conn-> query($queryString);
if( !$result ){
    echo "SQL Error.. ". $conn -> error." query: ".$queryString;
}
if( mysqli_num_rows($result) > 0 ){
?>

<?php
$computerCount = 0;
while( $row = $result->fetch_assoc() ){
    $statusSelect = getComputerStatusSelect("comp_status_".$computerCount, true, $row['status']);
    $locationSelect = getLocationSelect($conn, "comp_loc_".$computerCount, true, $row['location_id'] )

    ?>
    <tr>
        <td><input type='checkbox' id='compChk_<?php echo $computerCount;?>' value='<?php echo $row['id'];?>' disabled='true'></td>
        <td><a id='comp_id_<?php echo $computerCount; ?>' href='computer_details.php?computer_id=<?php echo $row['id']; ?>'><?php echo $row['id']; ?></td>
        <td><?php echo $statusSelect; ?></td>
        <td><input type='date' id='comp_distro_date_<?php echo $computerCount; ?>' value='<?php echo $row['distribution_date']; ?>' readonly></td>
        <td><?php echo $row['type']; ?></td><td><?php echo $locationSelect; ?></td>
    </tr>
    <?php
    $computerCount ++;
}
?>
</table>

<div id="editReservationWrapper"><button id="btn_edit_res" type="button" onclick="doEditReservation()">Edit Reservations</button></div>
<div id="editingReservationWrapper" style="display: none;"><button id="btn_update_res" type="button" onclick="doCancelReservation()">Remove Reservation(s)</button>
<button id="btn_update_res" type="button" onclick="doComputersUpdate()">Update</button>
<button id="btn_update_res" type="button" onclick="doCancelEdit()">Undo</button></div>
<div id="loader" class="loader" style="display: none;"></div>
</div>
<?php
}
?>
<script>
var computerCount = <?php echo $computerCount; ?>;

function doEditReservation(){
    showEditReservation(true);

}

function doCancelEdit(){
    showEditReservation(false);
}

function doCancelReservation(){
    showLoad(true);
    showEditReservation(true);
    document.getElementById("btn_update").disabled=true;

  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      //document.getElementById("response").innerHTML = this.responseText;
      var result = JSON.parse(this.responseText);
      // document.getElementById("result").innerHTML = result;
      // return;
      if( result.ret == "-1" )
      {
        document.getElementById("result").innerHTML = result.message;
        setReadOnly(false);
      }
      else {
        location.reload();
      }
      showLoad(false);
    }
  };

  xhttp.open("POST", "do_reserve_computer.php", true);
  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  var sendString = "";
  sendString+=<?php echo $requestId;?>;s
  sendString+="&computerIds=";
  var found = false;
  for( var i = 0; i < computerCount; i++){
      if( document.getElementById("compChk_"+i).checked == true ){
        if( found){
            sendString+=",";
        }
        found = true;
        sendString += document.getElementById("compChk_"+i).value;
      }
  }
  xhttp.send(sendString);
}

function doComputersUpdate(){
    showLoad(true);
    showEditReservation(true);
    document.getElementById("btn_update").disabled=true;

  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      //document.getElementById("response").innerHTML = this.responseText;
      //document.getElementById("result").innerHTML = this.responseText;
      //return;
      var result = JSON.parse(this.responseText);
      // document.getElementById("result").innerHTML = result;
      // return;
      if( result.ret == "-1" )
      {
        document.getElementById("result").innerHTML = result.message;
        setReadOnly(false);
      }
      else {
        location.reload();
      }
      showLoad(false);
    }
  };

  xhttp.open("POST", "do_computer_update_multiple.php", true);
  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  var sendString = "";
  sendString+="updateData=";
  var found = false;
  var updateArray = [];

  for( var i = 0; i < computerCount; i++){
      var row = {};
      row['id'] = document.getElementById("comp_id_"+i).innerHTML;
      row['status'] = "'"+document.getElementById("comp_status_"+i).value+"'";
      row['location_id'] = document.getElementById("comp_loc_"+i).value;
      row['distribution_date'] = "'"+document.getElementById("comp_distro_date_"+i).value+"'";

      updateArray.push( row );
  }
  sendString+= JSON.stringify(updateArray); 
  xhttp.send(sendString);
}

function showEditReservation(show){

    for( var i = 0 ; i < computerCount; i++){
        document.getElementById("compChk_"+i).disabled = !show;
        document.getElementById("comp_loc_"+i).disabled = !show;
        document.getElementById("comp_status_"+i).disabled = !show;
        document.getElementById("comp_distro_date_"+i).readOnly = !show;
        //document.getElementById("compChk_"+i).checked = false;
    }
    if(show)
    {
        document.getElementById("editingReservationWrapper").style.display = "inline";
        document.getElementById("editReservationWrapper").style.display = "none";
    }
    else{
        document.getElementById("editingReservationWrapper").style.display = "none";
        document.getElementById("editReservationWrapper").style.display = "inline";
    } 
}
</script>

