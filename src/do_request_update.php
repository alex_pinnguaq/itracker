<?php

include "helpers.php";
include "request_helpers.php";

$conn = db_connect();
$tableValueStartIndex = 4;
$valueNameList = ["id", "updateComputers", "updateComputersDate", "distribution_date","status", "requested_date", "request_deadline", "number", "notes"];
$valuesToBeChangedList = [];
$resArray = array();
$debug = false;
$vals = array();
for( $i = 0; $i<count($valueNameList); $i++){
    if( !array_key_exists($valueNameList[$i], $_POST)){
        //echo "Missing post arg: ".$valueNameList[$i];
        $resArray['ret']=-1;
        $resArray['message']="Missing post arg: ".$valueNameList[$i];
        echo json_encode($resArray);
        return;
    }
	else{
		if (($valueNameList[$i] == "request_deadline" || $valueNameList[$i] == "requested_date") && $_POST[$conn->real_escape_string($valueNameList[$i]) ] == '')
			{
			
			}
		else
		{
		array_push($valuesToBeChangedList, $valueNameList[$i]);
		array_push($vals, $conn->real_escape_string($_POST[$valueNameList[$i]]) );	
		}
	}
	
    
}


$query = "UPDATE request SET ";

for($i = $tableValueStartIndex; $i < count($valuesToBeChangedList); $i++){
    $query.=$valuesToBeChangedList[$i]." = '".$vals[$i]."'";
    if( $i < count($valuesToBeChangedList) - 1){
        $query.=",";
    }
}
$query.=" WHERE id=".$vals[0];


$result = $conn->query($query);
if( !$result){
    //echo "Insertion error: ".$conn->error." Query:".$query;
    $resArray['ret']=-1;
    $resArray['message']="Insertion error: ".$conn->error." Query:".$query;
    echo json_encode($resArray);
    return;
}


if( $vals[1] == 1 ){
    // TODO: Dynamically get "Distributed" status. 
    $query = "UPDATE computers SET status='Distributed'";
    if( $vals[2] == 1){
        $query.=", distribution_date='".$vals[3]."'";
    }
    $query.=" WHERE status <> 'Distributed' AND request_id = ".$vals[0];
    $result = $conn->query($query);
    if( !$result){
        //echo "Insertion error: ".$conn->error." Query:".$query;
        $resArray['ret']=-1;
        $resArray['message']="Insertion error: ".$conn->error." Query:".$query;
        echo json_encode($resArray);
        return;
    }
}

$resArray['ret']=$conn->insert_id;
$resArray['message']="Success";
echo json_encode($resArray);
return;