
<?php

include "header.php";
include "helpers.php";
checkRole(0);


?>
<script>
function doCreate(){
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      if( this.responseText == "-1" )
      {
        printError("Unable to Create User.");
      }
      else if (this.responseText == "-2"){
          printError("User already exists");
      }
      else if (this.responseText == "1" ){
        printError("User Created Successfully");
      }
      else {
          printError(this.responseText);
      }
    }
  };
  xhttp.open("POST", "user_do_create.php", true);
  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  var sendString = "username="+encodeURIComponent(document.getElementById("email").value);
  sendString+="&pass="+encodeURIComponent(document.getElementById("pass").value);
  sendString+="&fname="+encodeURIComponent(document.getElementById("fname").value);
  sendString+="&lname="+encodeURIComponent(document.getElementById("lname").value);
  sendString+="&regday="+encodeURIComponent(document.getElementById("regday").value);
  xhttp.send(sendString);
}

function printError(errorString)
{
  document.getElementById("response").innerHTML = errorString;
}


</script>


<form class="create_user">
Email:<input type="text" id="email"><br>
Password:<input type="text" id="pass" ><br>
First Name:<input type="text" id="fname"><br>
Last Name:<input type="text" id="lname"><br>
Day:<select id="regday" name="regday">
  <option value="-1">Flexible</option>
  <option value="0">Monday</option>
  <option value="1">Tuesday</option>
  <option value="2">Wednesday</option>
  <option value="3">Thursday</option>
  <option value="4">Friday</option>
  <option value="5">Saturday</option>
  <option value="6">Sunday</option>
</select>

<button type="button" onclick="doCreate()" >Create</button>
</form>
<div id="response"></div>

