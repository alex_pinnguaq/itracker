<?php

include "header.php";
include "helpers.php";
include "dropdown_helper.php";
?>
<h1>Insert new computer</h1>
<form id="new_computer_form">
<p>Type: <input type="text" name = "type" id="type"></p>
<p>Status: <?php echo create_status_dropdown() ?></p>
<p>Location: <?php echo create_location_dropdown() ?></p>
<p>Distribution Date: <input type="date" name = "distribution_date" id="distribution_date"></p>
<p>Notes: <textarea form="new_computer_form" rows="5" type="text" name = "notes" id="notes"></textarea>
<p>Manufacturer: <input type="text" name = "manufacturer" id="manufacturer"></p>
<p>Date Refurbished: <input type="date" name = "date_refurbished" id="date_refurbished"></p>


<button id="btn_submit" type="button" onclick="doComputerCreate()">Create</button>
</form>
<div id="loader" class="loader" style="display: none;"></div>
<div id="result"></div>
<script>
function doComputerCreate() {
  showLoad(true);
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      //document.getElementById("response").innerHTML = this.responseText;
      var result = JSON.parse(this.responseText);
      // document.getElementById("result").innerHTML = result;
      // return;
      if( result.ret == "-1" )
      {
        document.getElementById("result").innerHTML = result.message;
      }
      else {
        document.getElementById("result").innerHTML = "Computer Created Successfully!";
        window.location.replace("computer_details.php?computer_id="+result.ret);
      }
      showLoad(false);
    }
  };
  xhttp.open("POST", "insert_computer_details.php", true);
  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  var sendString = "";
  sendString+="&status="+encodeURIComponent(document.getElementById("status").value);
  sendString+="&type="+encodeURIComponent(document.getElementById("type").value);
  sendString+="&location="+encodeURIComponent(document.getElementById("location").value);
  sendString+="&distribution_date="+encodeURIComponent(document.getElementById("distribution_date").value);
  sendString+="&notes="+encodeURIComponent(document.getElementById("notes").value);
  sendString+="&manufacturer="+encodeURIComponent(document.getElementById("manufacturer").value);
  sendString+="&date_refurbished="+encodeURIComponent(document.getElementById("date_refurbished").value);
  
  xhttp.send(sendString);
}

function showLoad(show){
  if(show)
  {
    document.getElementById("loader").style.display = "block";
  }
  else{
    document.getElementById("loader").style.display = "none";
  }
}

</script>