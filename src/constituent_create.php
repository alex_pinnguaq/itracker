<?php

include "header.php";
include "helpers.php";

?>

<form>
<p>First Name: <input type="text" name = "fname" id="fname"></p>
<p>Last Name: <input type="text" name = "lname" id="lname"></p>
<p>Email: <input type="text" name = "email" id="email"></p>
<p>Phone: <input type="text" name = "phone" id="phone"></p>
<p>Address: <textarea id="address" name="address" rows="4" cols="50"></textarea></p>

<button id="btn_submit" type="button" onclick="doConstituentCreate()">Create</button>
</form>
<div id="loader" class="loader" style="display: none;"></div>
<div id="result"></div>
<script>
function doConstituentCreate() {
  showLoad(true);
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      //document.getElementById("response").innerHTML = this.responseText;
      var result = JSON.parse(this.responseText);
      // document.getElementById("result").innerHTML = result;
      // return;
      if( result.ret == "-1" )
      {
        document.getElementById("result").innerHTML = result.message;
      }
      else {
        document.getElementById("result").innerHTML = "Constituent Created Successfully!";
        window.location.replace("constituent.php?computer_id ="+result.ret);
      }
      showLoad(false);
    }
  };
  xhttp.open("POST", "do_constituent_create.php", true);
  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  var sendString = "";
  sendString+="&fname="+encodeURIComponent(document.getElementById("fname").value);
  sendString+="&lname="+encodeURIComponent(document.getElementById("lname").value);
  sendString+="&email="+encodeURIComponent(document.getElementById("email").value);
  sendString+="&phone="+encodeURIComponent(document.getElementById("phone").value);
  sendString+="&address="+encodeURIComponent(document.getElementById("address").value);
  xhttp.send(sendString);
}

function showLoad(show){
  if(show)
  {
    document.getElementById("loader").style.display = "block";
  }
  else{
    document.getElementById("loader").style.display = "none";
  }
}

</script>