<?php
  include "header.php";
  include "helpers.php";
?>
<script>
var badPassCount = 0;
function doLogin(){
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      if( this.responseText == "-1" )
      {
        badPassCount ++;
        
        var errString = "Invalid email or password.";
        if( badPassCount >= 2)
        {
          errString += "<br>To reset your password, please contact <?php echo getAdminEmail();?>";
        }
        printError(errString);
      }
      else if ( this.responseText == "-2" )
      {
        printError( "Your account is not active. Please contact admin at <?php echo getAdminEmail();?>" );      }
      else if (this.responseText == "1" ){
        printError("Login Successful");
        window.location.replace("user.php");
        // Go to user page.
      }
      else {
          printError(this.responseText);
      }
    }
  };
  xhttp.open("POST", "do_login.php", true);
  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  var sendString = "username="+encodeURIComponent(document.getElementById("email").value);
  sendString+="&pass="+encodeURIComponent(document.getElementById("pass").value);
  xhttp.send(sendString);
}

function printError(errorString)
{
  document.getElementById("response").innerHTML = errorString;
}


</script>


<form class="login">
email:<input type="text" id="email">
password:<input type="password" id="pass" >
<button type="button" onclick="doLogin()" >Login</button>
</form>
<div id="response"></div>
