<?php

include "header.php";
include "helpers.php";

$conn = db_connect();


$query = "SELECT * FROM location";

$result = $conn->query($query);
if(!$result){
    echo SQLErrorToString($query, $conn);
}
?>
<h2>Locations</h2>
<ul>
<?php 

while( $row = $result->fetch_assoc()){
    echo "<li>".$row['name']." - <a href='location_edit.php?id=".$row['id']."'>edit</a></li>";
    ?>
    
    <?php
}

?>
</ul>
<a href='location_add.php'>Add Location</a>
<?php
?>