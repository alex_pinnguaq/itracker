<?php

include "helpers.php";

$conn = db_connect();

$valueNameList = ["status", "type", "location", "distribution_date", "notes","manufacturer","date_refurbished"];
$resArray = array();
$debug = false;
$cols = "";
$vals = "";
for( $i = 0; $i<count($valueNameList); $i++){
    if( !array_key_exists($valueNameList[$i], $_POST)){
        //echo "Missing post arg: ".$valueNameList[$i];
        $resArray['ret']=-1;
        $resArray['message']="Missing post arg: ".$valueNameList[$i];
        echo json_encode($resArray);
        return;
    }
    $cols.=$valueNameList[$i];
	if($_POST[$valueNameList[$i]]!=""){
    $vals.="'".$conn->real_escape_string($_POST[$valueNameList[$i]])."'";
	}
	else{
		$vals.="NULL";
	}
    if( $i<count($valueNameList) -1 ){
        $cols.=",";
        $vals.=",";
    }
}


$query2 = "SELECT id FROM location WHERE name = '". $_POST['location'] ."'";


$result = $conn->query($query2);
$temp = $result -> fetch_assoc();
$location_id = $temp['id'];

$query = "INSERT INTO computers (".$cols." , location_id) VALUES (".$vals.", $location_id)";

$result = $conn->query($query);
if( !$result){
    //echo "Insertion error: ".$conn->error." Query:".$query;
    $resArray['ret']=-1;
    $resArray['message']="Insertion error: ".$conn->error." Query:".$query;
    echo json_encode($resArray);
    return;
}

$resArray['ret']=$conn->insert_id;
$resArray['message']="Success";
echo json_encode($resArray);
return;
?>	