<?php
include "../credentials.php";



function getSQLError($conn){
  return $conn->error;
}

function SQLErrorToString($query, $conn){
  return "SQL error: ".$conn->error." Query:".$query;
}

function getMaxRegistration(){
  return 25;
}

// This is the maximum number of users that can be evergreen registrations for a given date
function getMaxDayUsers(){
  return 24;
}

function getAdminEmail()
{
  $address = "alex.smithers@gmail.com";
  return "<a href='mailto:".$address."'>".$address."</a>";
}

function get_stylesheet()
{
  echo '<link rel="stylesheet" href="style.css">';
}

function error($errorString) {
  echo "<color ='red'>".$errorString."</color><br>";
}
function success($successString) {
  echo "<color ='green'>".$successString."</color><br>";
}

function getRole()
{
  if( !isset($_SESSION['userRole']))
  {
    return -1;
  }
  return $_SESSION['userRole'];
}

function getUserId(){
  if( !isset($_SESSION['userId']))
  {
    return -1;
  }
  return $_SESSION['userId'];
}

function getUserStatus($conn, $id){
  $queryString = "SELECT status FROM users WHERE id = ".$id;
  $result = $conn->query($queryString);
  if( $row = $result->fetch_assoc())
  {
    return $row['status'];
  }
  return -1;
}

function checkRole($targetRole){
  if( getRole()!=$targetRole)
  {
    header('Location: login.php');
    exit;
  }
}

function checkLoggedIn(){
  if( getRole() == -1){
    header('Location: login.php');
    exit;
  }
}

function getDayInfo($conn, $id){
  $queryString = "SELECT * FROM date LEFT JOIN media on media.id = date.image_id WHERE date.id = ".$id;
  //echo $queryString;
  $result = $conn->query($queryString);
  $row = $result->fetch_assoc();
  if(!$row)
  {
    return array();
  }
  return array( "dayNumber"=>$row['daynumber'], "image"=>$row['directory'].$row['filename'], "message"=>$row['message'],"date"=>$row['date']);
}

function getLunchDayInfo($conn, $id){
  $queryString = "SELECT * FROM lunches WHERE id = ".$id;
  //echo $queryString;
  $result = $conn->query($queryString);
  $row = $result->fetch_assoc();
  if(!$row)
  {
    return array();
  }
  return array(  "meal"=>$row['meal'],"date"=>$row['date']);
}

function displayTable($result){
  if( !$result ){
    echo "Error in query results.";
  }
  $outStr = "<table><tr>";
  $row = $result -> fetch_assoc();
  $keys = array_keys($row);
  for( $i = 0; $i < count($keys); $i++){
    $outStr .= "<td>".$keys[$i]."</td>";
  }
  $outStr .= "</tr>";
  while( $row ){
    $outStr .="<tr>";
    for( $i = 0; $i < count($keys); $i++){
      $outStr .= "<td>".$row[$keys[$i]]."</td>";
    }
    $outStr.="</tr>";
    $row = $result->fetch_assoc();
  }
  $outStr.="</table>";
  return $outStr;
}

function getRequestStatusOptions(){
  return array("Requested", "Reserved", "Processing", "Shipped", "Distributed");
}

$computerStatusList = array( "Unassessed", "Unwiped", "Hardware Assembly", "Basic Software Installation", "Special Software Installation", "Final Check", "Ready for Distribution", "Broken", "Distributed", "Scrapped", "On Loan", "TAGGED");


function getComputerStatusSelect($inputId, $readOnly=true, $selectedId=-1){
  
  global $computerStatusList;
  $outStr = "<select id='".$inputId."' ".($readOnly?"disabled='disabled'":"")." >";

  for($i = 0; $i < count($computerStatusList); $i++ ){
    //$outStr += $row['id'];

    //$outStr += $row['name'];
    $outStr .= "<option value='".$computerStatusList[$i]."' ".($selectedId == $computerStatusList[$i]?" selected='true'":"").">".$computerStatusList[$i]."</option>";
  }
  $outStr .= "</select>";
  return $outStr;
}

function getLocationSelect($conn, $inputId, $readOnly=true, $selectedId=-1){

  $query = "SELECT * FROM location ORDER BY name";
  $result = $conn->query($query);
  if(!$result){
    return "";
  }
  $outStr = "<select id='".$inputId."' ".($readOnly?"disabled='disabled'":"")." >";
  while( $row = $result->fetch_assoc()){
    //$outStr += $row['id'];

    //$outStr += $row['name'];
    $outStr .= "<option value='".$row['id']."' ".($selectedId == $row['id']?" selected='true'":"").">".$row['name']."</option>";
  }
  $outStr .= "</select>";
  return $outStr;
}

function getLocationStringToId($conn, $string){
  $query = "SELECT id FROM location WHERE name = '".$string."'";
  $result = $conn->query($query);
  if(!$result){
    return -1;
  }
  return $result->fetch_assoc()['id'];
}

// -------------- Settings ----------------

$maxUsers;



 ?>