<?php
include "helpers.php";

$conn = db_connect();

$resArray = array();
if( !array_key_exists("updateData", $_POST)){
    $resArray['ret']=-1;
    $resArray['message']="Missing POST argument 'updateData'.";
    echo json_encode($resArray);
    return;
}

$updateData = json_decode($_POST['updateData']);
for($i = 0; $i < count( $updateData); $i++){
    $keys= "";
    $values = "";
    $whereClause = " WHERE id = ";
    $query = "UPDATE computers SET";
    foreach($updateData[$i] as $key => $value){
        if( $key == 'id' ){
            $whereClause.="$value";
        }
        else{
            $query.=" ".$key."=".$value.",";
        }
    }
    $query = substr($query, 0, -1).$whereClause;

    $result = $conn->query($query);

    if( !$result){
        //echo "Insertion error: ".$conn->error." Query:".$query;
        $resArray['ret']=-1;
        $resArray['message']="Update Error: ".$conn->error." Query:".$query;
        echo json_encode($resArray);
        return;
    }

}

$resArray['ret']=$conn->insert_id;
$resArray['message']="Success";
echo json_encode($resArray);
return;



?>