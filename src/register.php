<?php
include "header.php";

$defaultDate = " new Date() ";

include "helpers.php";
checkLoggedIn();

$conn = db_connect();

?>
<h1>Registration</h1>
<h2>Your Registrations</h2>
<?php
$dayInfo = array();
$dayId = -1;
$selectedDayId = -1;
$startDate = "'".date('Y-m-d')." 00:00:00'";
if( isset($_GET['selectedDate'])){
  $startDate = "'".$_GET['selectedDate']." 00:00:00'";
}

$queryString = "select * from date";
$result = $conn->query($queryString);

$userId = getUserId();
$queryString = "SELECT * FROM registration INNER JOIN date ON registration.date_id = date.id WHERE date.date >= CURDATE() AND registration.user_id = ".$userId." ORDER BY date.date";
//$queryString = "SELECT * FROM registration INNER JOIN date ON registration.date_id = date.id WHERE registration.user_id = ".$userId;
$result = $conn->query($queryString);
$futureRegistration = -1;
$futureRegistrationString = "<p>You are currently registered for the following dates:</p>";
$futureRegistrationListString = "var userRegs = [";
$regCount = 0;
while ($row = $result -> fetch_assoc()) {
  $futureRegistration = $row['date_id'];
  $futureRegistrationString .= "<p><a onclick='selectDate(".$row['date_id'].")'>".$row['date']."</a></p>";
  if( $regCount != 0 ){
    $futureRegistrationListString.=",";
  }
  $futureRegistrationListString.=$row['date_id'];
  $regCount += 1;
}
$futureRegistrationListString.="];";

$dateDataOut = "<script>var dateData = [];\n";

$queryString = "SELECT date.id, date.date, date.start, date.end, IFNULL(q.count, 0) as count FROM date LEFT JOIN ( SELECT date_id, COUNT(*) as count FROM registration GROUP BY date_id ) q ON q.date_id = date.id";
$result = $conn->query($queryString);

while ($row = $result -> fetch_assoc()) {
  $dateDataOut.= "dateData['".$row['id']."'] = {date: '".$row['date']."', start: '".$row['start']."', end: '".$row['end']."', registered:'".$row['count']."'};\n";
  //echo "DATE COMP:".$row['date']." to ".substr( $startDate, 1, 10);
  if( $row['date'] == substr( $startDate, 1, 10)){
    $selectedDayId = $row['id'];
  }
}
$dateDataOut.="var nextRegUserDate = ".$futureRegistration.";";
$dateDataOut.=$futureRegistrationListString;
$dateDataOut.="</script>";
echo $dateDataOut;
echo $futureRegistrationString;
?>

 <div id="myCalendarWrapper" class="calendarWrapper"></div>
<link rel="stylesheet" href="calendar/CalendarPicker.style.css" />
<script src="calendar/CalendarPicker.js"></script>
<script>

var dataArray = [];
for( var key in dateData){
  var theDateOne = new Date(dateData[key].date + " 00:00:00");
  var theDateOneTime = theDateOne.getTime();
  dataArray[theDateOneTime] = key;
}

var startDate = new Date( <?php echo $startDate; ?> );

const myCalendar = new CalendarPicker('#myCalendarWrapper', {
  data: dataArray,
  startDate: startDate
      // options here
});

</script>
<div class = "dayContainer">
<div id="dayInfo" class="dayInfo"></div>
<button id="btn_register" onclick="register()">Register</button>
<button id="btn_unregister" onclick="unregister()" hidden="true">Un-register</button>


<?php
if( getRole() == 0){

?>
<h3>Admin</h3>
<button id="btn_delete" onclick="date_delete()" hidden="true">Delete Date</button>
<button id="btn_create" onclick="date_create()" hidden="true">Create Date</button>

<?php
}
?>
<div id="loader" class="loader"></div>
<div id="result"></div>
</div>
<script>


var curDate = "<?php echo date('Y-m-d'); ?>";
var curTime = "<?php echo date('H:i:s'); ?>";


function isRegistrationClosed(id){
  if( dateData[id].date < curDate || dateData[id].end < curTime ){
    return true;
  }
  return false;
}

var isAdmin = <?php echo json_encode(getRole()==0); ?>;

var selectedDayId = <?php echo $selectedDayId; ?>;
var selectedDate = '<?php echo substr( $startDate, 1, 10); ?>';
function onCalendarValueChange(currentValue, newSelectedDate){
  selectedDayId = currentValue;
  selectedDate = newSelectedDate;
  var infoString = "There is no registration available for the selected day.";
  if( selectedDayId >= 0){

    if(isRegistrationClosed(selectedDayId)){
      infoString = "Registration is closed for this day.<br>";
      document.getElementById("btn_register").hidden = true;
      document.getElementById("btn_unregister").hidden = true;
    }
    else if( userRegs.includes(parseInt(selectedDayId) )){
      infoString = "You are currently registered for this day.<br>";
      document.getElementById("btn_register").hidden = true;
      document.getElementById("btn_unregister").hidden = false;
    }
    else if( dateData[selectedDayId].registered>=25)
    {
      infoString = "Registration for this day is full.<br>";
      document.getElementById("btn_register").hidden = true;
      document.getElementById("btn_unregister").hidden = true;
    }
    else if(userRegs.length == 0 || isAdmin ) {
      infoString = "You are not currently registered for this day.";
      document.getElementById("btn_register").hidden = false;
      document.getElementById("btn_unregister").hidden = true;
    }
    else{
      infoString = "You may only register for one day a time.";
      document.getElementById("btn_register").hidden = true;
      document.getElementById("btn_unregister").hidden = true;
    }
    var currentRegistration = "<p>Current Registration: "+dateData[currentValue].registered+" / <?php echo getMaxRegistration(); ?><p>";
    infoString = currentRegistration + "<p>" + infoString + "</p>";
  }
  else{
    document.getElementById("btn_register").hidden = true;
      document.getElementById("btn_unregister").hidden = true;
  }

  if( isAdmin )
  {
    if( selectedDayId == -1)
    {
      document.getElementById("btn_delete").hidden = true;
      document.getElementById("btn_create").hidden = false;
    }
    else{
      document.getElementById("btn_delete").hidden = false;
      document.getElementById("btn_create").hidden = true;
    }
  }
  document.getElementById("dayInfo").innerHTML = infoString;

  

}

myCalendar.onValueChange(onCalendarValueChange);

function selectDate(dayId, selectedDate){
  if( dayId >= 0 ){
    myCalendar._setDate(new Date(dateData[dayId].date+" 00:00:00"));
  }
  else{
    myCalendar._setDate(new Date(selectedDate+" 00:00:00"));
  }
  myCalendar._updateDate();
  onCalendarValueChange(dayId, selectedDate);
  
}
selectDate( selectedDayId, selectedDate);
//onCalendarValueChange(selectedDayId);


</script>


<script>



function showLoad(show){
  if(show)
  {
    document.getElementById("loader").style.display = "block";
  }
  else{
    document.getElementById("loader").style.display = "none";
  }
}

function register() {
  showLoad(true);
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      //document.getElementById("response").innerHTML = this.responseText;
      var result = this.responseText;
      if( result == "-1" )
      {
        document.getElementById("result").innerHTML = "Registration Failed: Date Full";
      }
      else if (result == "-2")
      {
        document.getElementById("result").innerHTML = "Registration Failed: You are already registered for a future date.";
      }
      else if (result == "-3")
      {
        document.getElementById("result").innerHTML = "Registration Failed: SQL Error - Contact admin.";
      }
      else if (result == "-4"){
        document.getElementById("result").innerHTML = "Registration Failed: Invalid Session or POST data. Please Refresh.";
      }
      else if (result == "-5"){
        document.getElementById("result").innerHTML = "Registration Failed: No Registration Available for selected date.";
      }
      else if (result == "-6"){
        document.getElementById("result").innerHTML = "Registration Failed: Registration is closed for this date.";
      }
      else if (result == "-7"){
        document.getElementById("result").innerHTML = "Registration Failed: You are already registered for the selected date.";
      }
      else if (result == "-8"){
        document.getElementById("result").innerHTML = "Registration Failed: Your account is not active.";
      }
      else if (result == "1"){
        document.getElementById("result").innerHTML = "Registration Successful!";
        window.location = "register.php?selectedDate="+dateData[selectedDayId].date;
      }
      else {
        document.getElementById("result").innerHTML = result;
      }
      showLoad(false);
    }
  };
  xhttp.open("POST", "do_register.php", true);
  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

  var sendString = "dayId="+encodeURIComponent(selectedDayId);
  xhttp.send(sendString);
}

function unregister() {
  showLoad(true);
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      //document.getElementById("response").innerHTML = this.responseText;
      var result = this.responseText;
      if( result == "-1" )
      {
        document.getElementById("result").innerHTML = "Unregistration Failed: You are not registered for this date.";
      }
      else if (result == "-2")
      {
        document.getElementById("result").innerHTML = "Unregistration Failed: SQL Error.";
      }
      else if (result == "-3")
      {
        document.getElementById("result").innerHTML = "Unregistration Failed: Invalid Session or POST data. Please Refresh.";
      }
      else if (result == "1"){
        document.getElementById("result").innerHTML = "Unregistration Successful!";
        window.location = "register.php?selectedDate="+dateData[selectedDayId].date;
      }
      else {
        document.getElementById("result").innerHTML = result;
        
      }
      showLoad(false);
    }
  };
  xhttp.open("POST", "do_unregister.php", true);
  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

  var sendString = "dayId="+encodeURIComponent(selectedDayId);
  xhttp.send(sendString);
}

function date_create() {
  showLoad(true);
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      //document.getElementById("response").innerHTML = this.responseText;
      var result = this.responseText;
      if( result == "-1" )
      {
        document.getElementById("result").innerHTML = "Unable to create date. Date already Exists.";
      }
      else if (result == "-2")
      {
        document.getElementById("result").innerHTML = "Unable to create Date. You do not have the required permissions.";
      }
      else if (result == "-3")
      {
        document.getElementById("result").innerHTML = "Unable to create date. SQL Error.";
      }
      else if (result == "1"){
        document.getElementById("result").innerHTML = "Date created successfully!";
        window.location = "register.php?selectedDate="+selectedDate;
      }
      else {
        document.getElementById("result").innerHTML = result;
        
      }
      showLoad(false);
    }
  };
  xhttp.open("POST", "do_day_create.php", true);
  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

  var sendString = "date="+encodeURIComponent(selectedDate);
  xhttp.send(sendString);
}

function date_delete() {
  showLoad(true);
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      //document.getElementById("response").innerHTML = this.responseText;
      var result = this.responseText;
      if( result == "-1" )
      {
        document.getElementById("result").innerHTML = "Unable to delete date. Date does not exists.";
      }
      else if (result == "-2")
      {
        document.getElementById("result").innerHTML = "Unable to delete Date. You do not have the required permissions.";
      }
      else if (result == "-3")
      {
        document.getElementById("result").innerHTML = "Unable to delete date. SQL Error.";
      }
      else if (result == "1"){
        document.getElementById("result").innerHTML = "Date deleted successfully!";
        window.location = "register.php?selectedDate="+selectedDate;
      }
      else {
        document.getElementById("result").innerHTML = result;
        
      }
      showLoad(false);
    }
  };
  xhttp.open("POST", "do_day_delete.php", true);
  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

  var sendString = "id="+encodeURIComponent(selectedDayId);
  xhttp.send(sendString);
}

</script>

<div id="response"></div>

<script>
showLoad(false);
</script>