<?php

include "header.php";
include "helpers.php";
include "constituent_helpers.php";
include "request_helpers.php";

echo "<link rel=\"stylesheet\" href=\"table.css\">";
$conn = db_connect();

// Create connection

$queryString = "SELECT request.id, request.status, request.requested_by, request.requested_date, request.request_deadline, request.number, constituent.fname, constituent.lname FROM request INNER JOIN constituent ON request.requested_by = constituent.id";
echo "<a href='request_create.php'>Create</a>";

$result = $conn-> query($queryString);
if( !$result ){
    echo "SQL Error.. ". $conn -> error. " on query ".$queryString;
}
// $tableStr = displayContituentTable( $result);
// echo $tableStr;
// id	fname	lname	email	phone	address

$headings = array("Request Id","Requester","Status","Requested", "Due", "Number");

$outStr = "<table><tr>";
for( $i = 0; $i < count($headings); $i++){
    $outStr .= "<th>".$headings[$i]."</th>";
}
$outStr .= "</tr>";


while( $row = $result->fetch_assoc() ){
    $outStr .="<tr>";
    $outStr.="<td><a href='request.php?id=".$row['id']."'>".$row['id']."</a></td>";
    $outStr.="<td><a href='constituent.php?id=".$row['requested_by']."'>".$row['fname']." ".$row['lname']."</td>";
    $outStr.="<td>".requestStatusToString($row['status'])."</td>";
    $outStr.="<td>".$row['requested_date']."</td>";
    $outStr.="<td>".$row['request_deadline']."</td>";
    $outStr.="<td>".$row['number']."</td>";
    $outStr.="</tr>";
}
$outStr.="</table>";
echo $outStr;


 ?>
